import os, json, subprocess
from bottle import route, run, template, error, request, redirect
from os import listdir
from os.path import isfile, join
from tinytag import TinyTag
from collections import OrderedDict

html_escape_table = {
    "&": "&amp;",
    '"': "&quot;",
    "'": "&apos;",
    ">": "&gt;",
    "<": "&lt;"
}

def html_escape(text):
     return "".join(html_escape_table.get(c,c) for c in text)


config_file = "/etc/wonderfoon/config.json"
mode_types = ['wonderfoon','escaperoom']
index_html = '''My first web app! By <strong>{{ author }}</strong>.'''

html_head = '''<html>
    <head>
        <title>Wonderfoon setup</title>
        <style>
            table {
                border-collapse: collapse
            }

            td, th {
                border: 1px solid #dddddd;
                text-align: left;
                padding: 5px;
            }

            tr:nth-child(even) {
                background-color: #dddddd;
            }
        </style>
    </head>
    <body>
        <h2>Wonderfoon setup</h2>
        <a href="/assign">Nummers toewijzen</a> - <a href="/library">Muziek bestanden beheren</a> - <a href="/config">Instellingen</a>
        <br><br>\n'''

@route ('/config')
def config():
	try:
		with open(config_file) as configuration:
			config = json.load(configuration)
	except:
		config = {}
		config["Mode"] = "wonderfoon"
		config["Shutdown"] = "999"
		config["GetIP"] = "127"
		config["WifiReset"] = "123654"
		config["Volume"] = "80"
		config["MusicDir"] = "/usr/share/wonderfoon/music/"
	output = html_head
	output += '''\t\t<form action="/config" method="post">
			<table>
	'''
	output += '\t\t\t\t<tr>\n\t\t\t\t\t<td>Volume:</td>\n\t\t\t\t\t<td><input type="range" name="Volume" min="0" max="100" value="' + config['Volume'] + '"></td>\n\t\t\t\t</tr>\n'
	i=0
	output += '\t\t\t\t<tr>\n\t\t\t\t\t<td>Mode:</td>\n\t\t\t\t\t<td>\n\t\t\t\t\t\t<select name="Mode">\n'
	while i < len(mode_types):
		if mode_types[i] == config['Mode']:
			output += '\t\t\t\t\t\t\t<option value="' + mode_types[i] + '" selected>' + mode_types[i] + '</option>\n'
		else:
			output += '\t\t\t\t\t\t\t<option value="' + mode_types[i] + '">' + mode_types[i] + '</option>\n'
		i += 1
	output += '\t\t\t\t\t\t</select>\n\t\t\t\t\t</td>\n\t\t\t\t</tr>\n'
	output += '\t\t\t\t<tr>\n\t\t\t\t\t<td>Uitschakelen:</td>\n\t\t\t\t\t<td><input type="inut" name="Shutdown" size="30" value="' + config['Shutdown'] + '"></td>\n\t\t\t\t</tr>\n'
	output += '\t\t\t\t<tr>\n\t\t\t\t\t<td>IP oplezen:</td>\n\t\t\t\t\t<td><input type="input" name="GetIP" size="30"  value="' + config['GetIP'] + '"></td>\n\t\t\t\t</tr>\n'
	#output += '\t\t\t\t<tr>\n\t\t\t\t\t<td>WiFi reset:</td>\n\t\t\t\t\t<td><input type="input" name="WifiReset" size="30"  value="' + config['WifiReset'] + '"></td>\n\t\t\t\t</tr>\n'
	output += '\t\t\t\t<tr>\n\t\t\t\t\t<td>MusicDir:</td>\n\t\t\t\t\t<td><input type="input" name="MusicDir" size="30"  value="' + config['MusicDir'] + '"></td>\n\t\t\t\t</tr>\n'
	output += '\t\t\t\t<tr>\n\t\t\t\t\t<td colspan="2"><button type="submit">Opslaan</button></td>\n\t\t\t\t</tr>\n'
	output += '''\t\t\t</table>
		</form>
	</body>
</html>'''
	return output

@route ('/config', method='POST')
def config_save():
	config={}
	for item in request.forms:
		config[item]=request.forms.get(item)
	json_config = json.dumps(config, sort_keys=True, indent=2)
	f = open(config_file, "w")
	f.write(json_config)
	f.close()
	subprocess.Popen(["/usr/bin/amixer", "cset", "numid=1",  str(config["Volume"]) + "%"])
	redirect("/config")

@route ('/library')
def library():
	try:
		with open(config_file) as configuration:
			config = json.load(configuration)
	except:
		redirect("/config")
	output = html_head
	songlist = {}
	allsongs = [f for f in listdir(config["MusicDir"]) if isfile(join(config["MusicDir"], f))]
	for x in allsongs:
		if x != 'kiestoon.ogg':
			tag = TinyTag.get(config["MusicDir"] + x)
			songlist[x]={'title': tag.title, 'artist': tag.artist}
	output += '''\t\t<form action="/library" method="POST" onSubmit="if(!confirm('Weet je het zeker? Verwijderde bestanden kunnen niet worden hersteld!')){return false;}">
			<table>
				<tr>
					<td></td>
					<td>Bestand</td>
					<td>Artiest</td>
					<td>Titel</td>
				</tr>\n'''
	for key,val in sorted(songlist.items()):
		if (str(val["artist"]) == 'None') or (str(val["title"]) == 'None'):
			output += '\t\t\t\t<tr>\n\t\t\t\t\t<td><input type="checkbox" name="' + key + '"></td>\n\t\t\t\t\t<td>' + key + '</td>\n\t\t\t\t\t<td> unknown </td>\n\t\t\t\t\t<td> unknown </td>\n\t\t\t\t</tr>\n'
		else:
			output += '\t\t\t\t<tr>\n\t\t\t\t\t<td><input type="checkbox" name="' + key + '"></td>\n\t\t\t\t\t<td>' + key + '</td>\n\t\t\t\t\t<td>' + html_escape(str(val["artist"])) + '</td>\n\t\t\t\t\t<td>' + html_escape(str(val["title"])) + '</td>\n\t\t\t\t</tr>\n'
	output += '\t\t\t\t<tr>\n\t\t\t\t\t<td colspan="4"><button type="submit">Verwijderen</button><b>Pas op! Deze actie is onomkeerbaar!</b></td>\n\t\t\t\t</tr>\n'
	output += '''\t\t\t</table>
		</form>
		<form action="/upload" method="POST" enctype="multipart/form-data">
			<table>\n'''
	output += '\t\t\t\t<tr>\n\t\t\t\t\t<td colspan="2"><input type="file" name="upload" accept=".mp3,.ogg,.wav"></td>\n\t\t\t\t\t<td><button type="submit">Opslaan</button></td>\n\t\t\t\t</tr>\n'
	output += '''\t\t\t</table>
		</form>
	</body>
</html>'''
	return output

@route ('/library', method='POST')
def library_save():
	with open(config_file) as configuration:
		config = json.load(configuration)
	data={}
	for item in request.forms:
		data[item]=request.forms.get(item)
	for x in data.keys():
		os.remove(config["MusicDir"] + x)
	redirect("/library")

@route ('/upload', method='POST')
def library_upload():
	with open(config_file) as configuration:
		config = json.load(configuration)
	upload     = request.files.get('upload')
	name, ext = os.path.splitext(upload.filename)
	if ext not in ('.mp3','.ogg','.wav'):
		return 'Verkeerd bestandsformaat. .mp3, .ogg, .wav zijn toegestaan'
	upload.save(config["MusicDir"])
	print(name)
	redirect("/library")

@route('/assign')
def assign():
	count = 0
	try:
		with open(config_file) as configuration:
			config = json.load(configuration)
	except:
		redirect("/config")
	songlist = {}
	allsongs = [f for f in listdir(config["MusicDir"]) if isfile(join(config["MusicDir"], f))]
	for x in allsongs:
		tag = TinyTag.get(config["MusicDir"] + x)
		if (str(tag.title) == 'None') or (str(tag.artist) == 'None'):
			songlist[x]={'title': x, 'artist': x}
		else:
			songlist[x]={'title': tag.title, 'artist': tag.artist}
	if len(allsongs) == 0:
		redirect("/library")
	output = html_head
	output += '''\t\t<form action="/assign" method="POST">
			<table id="Assign">'''
	if config['Mode'] == 'wonderfoon':
		output += '\t\t\t\t<tr>\n\t\t\t\t\t<td colspan="2">In Wonderfoon mode zullen slechts de enkele getallen worden gebruikt! (0-9)</td>\n\t\t\t\t</tr>\n'
		output += '\t\t\t\t<tr>\n\t\t\t\t\t<th>Nummer</th>\n\t\t\t\t\t<th>\n\t\t\t\t\t\tGeluidsbestand</th>\n\t\t\t\t</tr>\n'
	try:
		with open('/etc/wonderfoon/music.json') as music_file:
			musiclist = json.load(music_file, object_pairs_hook=OrderedDict)
		for k,v in sorted(musiclist.items()):
			output += '\t\t\t\t<tr>\n\t\t\t\t\t<td><input type="text" size="8" name="num' + str(count) + '" value="' + str(k) + '"></td>\n\t\t\t\t\t<td>\n\t\t\t\t\t\t<select name="song' + str(count)  + '">\n'
			for key,val in sorted(songlist.items()):
				if str(v) == str(key):
						output += '\t\t\t\t\t\t\t<option value="' + str(key) + '" selected>' + html_escape(str(val["artist"])) + ' - ' + html_escape(str(val["title"])) + '</option>\n'
				else:
					output += '\t\t\t\t\t\t\t<option value="' + str(key) + '">' + str(val["artist"]) + ' - ' +str(val["title"]) + '</option>\n'
			output += '\t\t\t\t\t\t</select>\n\t\t\t\t\t</td>\n\t\t\t\t</tr>\n'
			count += 1
	except:
		while count<len(songlist):
			output += '\t\t\t\t<tr>\n\t\t\t\t\t<td><input type="text" size="8" name="num' + str(count) + '" value="' + str(count) + '"></td>\n\t\t\t\t\t<td>\n\t\t\t\t\t\t<select name="song' + str(count)  + '">\n'
			for key,val in sorted(songlist.items()):
				output += '\t\t\t\t\t\t\t<option value="' + str(key) + '">' + html_escape(str(val["artist"])) + ' - ' + html_escape(str(val["title"])) + '</option>\n'
			output += '\t\t\t\t\t\t</select>\n\t\t\t\t\t</td>\n\t\t\t\t</tr>\n'
			count += 1
	#	return "oeps!"
	output += '\t\t\t\t<tr>\n\t\t\t\t\t<td colspan="2"><button type="submit">Opslaan</button></td>\n\t\t\t\t</tr>\n'
	select = '<select name=\'song" + c + "\'>'
	for key,val in sorted(songlist.items()):
		select += '\t\t\t\t\t\t\t<option value=\'' + str(key) + '\'>' + html_escape(str(val["artist"])) + ' - ' + html_escape(str(val["title"])) + '</option>'
	output += '''\t\t\t</table>
		</form>
		<button onclick="extraRow()">Nummer toevoegen</button>
		<script>\n'''
	output += '\t\t\t\tvar c = ' + str(count) + ';\n'
	output += '''\t\t\t\tfunction extraRow() {
				var table = document.getElementById("Assign");
				var rowCount = table.rows.length;
				var row = table.insertRow(rowCount-1);
				var cell1 = row.insertCell(0);
				var cell2 = row.insertCell(1);'''
	output += 'cell1.innerHTML = "<input size=\'8\' name=\'num" + c + "\' type=\'text\'>";\n'
	output += 'cell2.innerHTML = "' + select + '</select>";'
	output += 'c++;'
	output +='''}
		</script>
	</body>
</html>'''
	return output

@route('/assign', method='POST')
def assign_save():
	config={}
	for item in request.POST:
		config[item]=request.forms.get(item)
	playlist = {}
	for key,val in config.items():
		if key.startswith('num'):
			if val == 'del':
				continue
			k = key.strip('num')				
			song = 'song' + k
			playlist[val] = config[song]
	json_config = json.dumps(playlist, sort_keys=True, indent=2)
	f = open("/etc/wonderfoon/music.json", "w")
	f.write(json_config)
	f.close()
	redirect("/assign")

@route('/wifi')
def wifi():
	process = subprocess.Popen(['sudo', 'iwlist', 'wlan0', 'scan'], stdout=subprocess.PIPE)
	out, err = process.communicate()
	return out

@route('/')
def default():
	redirect('/assign')

@route('/name/<name>')
def name(name):
	return template(index_html, author=name)

@error(404)
def error404(error):
	return 'Page not found... <br><br><a href="/">Home</a>'

if __name__ == '__main__':
	run(host="0.0.0.0", port=80, debug=True, reloader=True)