#!/bin/sh
apt update
apt -y upgrade
apt -y install python3-pip dnsmasq hostapd
mkdir /etc/wonderfoon
chown pi /etc/wonderfoon

pip3 install -r requirements.txt
cp wonderfoon-webinterface.service /etc/systemd/system/
cp hostapd.conf /etc/hostapd/hostapd.conf
systemctl daemon-reload
systemctl enable wonderfoon-webinterface.service
systemctl start wonderfoon-webinterface.service

echo "192.168.4.1	wonderfoon.localnet wonderfoon" >> /etc/hosts
echo "interface wlan0" >> /etc/dhcpcd.conf
echo "    static ip_address=192.168.4.1/24" >> /etc/dhcpcd.conf
echo "    nohook wpa_supplicant" >> /etc/dhcpcd.conf
echo "local=/localnet/" >> /etc/dnsmasq.conf
echo "domain=localnet" >> /etc/dnsmasq.conf
echo "interface=wlan0" >> /etc/dnsmasq.conf
echo "  dhcp-range=192.168.4.2,192.168.4.20,255.255.255.0,24h" >> /etc/dnsmasq.conf
MAC=`ip l show wlan0 | grep ether | cut -d ' ' -f 6 | sed 's/://g'`
sed -i "s/NameOfNetwork/WF-$MAC/" /etc/hostapd/hostapd.conf
echo 'DAEMON_CONF="/etc/hostapd/hostapd.conf"' >> /etc/default/hostapd
sudo systemctl unmask hostapd
sudo systemctl unmask dnsmasq
sudo systemctl enable hostapd
sudo systemctl enable dnsmasq
sudo systemctl start hostapd
sudo systemctl start dnsmasq